import React, {useState} from 'react';
import './AddMess.css';
import axiosOrders from "../../axios-orders";
import Spinner from "../../components/UI/Spiner/Spinner";

const AddMess = props => {
  const [customer, setCustomer] = useState({
    name: '',
    textarea: ''
  });

   const [loading, setLoading] = useState(false);

  const customerChanged = event => {
    const name = event.target.name;
    const value = event.target.value;

    setCustomer(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const orderHandler = async event => {
    event.preventDefault();
    setLoading(true);

    const message = {
      customer: {...customer}
    };

    try {
      await axiosOrders.post('/message.json', message);
    } finally {
      setLoading(false);
      props.history.replace('/');
    }
  };

  let form = (
      <form onSubmit={orderHandler}>
        <p>Title</p>
        <input
            className="add-title"
            name="name"
            type="text"
            placeholder="Enter title"
            value={customer.name}
            onChange={customerChanged}
        />
        <p>Description</p>
        <textarea
            className="add-text"
            name="textarea"
            placeholder="Enter your message"
            value={customer.textarea}
            onChange={customerChanged}
        />
        <button type="submit" className="add-btn">Save</button>
      </form>
  );

  if(loading){
    form = <Spinner/>;
  }


  return (
      <div className="add-block">
        <h2>Add new post</h2>
        {form}
      </div>
  );
};

export default AddMess;