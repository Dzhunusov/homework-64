import React from 'react';
import './Home.css';
import MessItem from "../../components/MessItem/MessItem";

const Home = props => {
  return (
      <div className="container">
          <MessItem
              message="Some message...."
          />
      </div>
  );
};

export default Home;