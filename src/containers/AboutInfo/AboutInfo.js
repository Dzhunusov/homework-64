import React from 'react';

const AboutInfo = () => {
  return (
      <>
        <h2>About Us</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sagittis leo lacus. Quisque tincidunt
          fermentum aliquam. Sed eleifend luctus mi, in auctor ex eleifend ut. Morbi placerat nec augue at consectetur.
          Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam augue enim,
          vulputate eu nunc non, porttitor scelerisque urna. Phasellus vestibulum malesuada est. Aenean nec turpis a
          diam laoreet imperdiet et tincidunt libero. Nam vel tortor sed sem auctor molestie sit amet vitae dui. Cras
          vitae ante eget augue mattis facilisis. Nunc et efficitur mauris. Curabitur at sapien finibus, iaculis nisi a,
          rutrum justo. Praesent orci quam, consequat ac tortor nec, convallis feugiat metus. Sed maximus pretium elit
          mattis ultricies. Praesent in rhoncus urna.</p>

        <p>In vehicula magna sit amet erat lacinia malesuada. Maecenas sit amet tellus bibendum, blandit purus non,
          fermentum leo. Duis pellentesque sit amet nibh vitae commodo. Vestibulum in mollis augue. Pellentesque vitae
          hendrerit nunc. Sed quis enim tempus, sagittis dui ac, hendrerit urna. Etiam egestas consectetur dui ut
          tincidunt. Phasellus sed metus eu risus pharetra lacinia in ac lectus. Curabitur at nisi tellus. Nam nec dolor
          id lorem egestas sagittis vel sit amet eros. Duis dictum nisi suscipit volutpat laoreet. Quisque condimentum
          ac sapien id faucibus. Aliquam justo elit, pretium ac sollicitudin ut, dapibus id risus.</p>

        <p>Cras a maximus dolor. Etiam consequat lectus eu quam dignissim, ac fermentum velit rutrum. Vivamus luctus ac
          arcu non suscipit. Duis rhoncus tellus vel magna convallis, id vehicula magna hendrerit. Cras risus nunc,
          imperdiet vitae nisi a, porta aliquet risus. Curabitur at faucibus nisi, quis tincidunt ante. Donec sed risus
          nisl. Proin rhoncus ultricies laoreet. Sed massa ante, elementum interdum dui nec, lobortis mollis libero.
          Vestibulum at suscipit dui, et finibus turpis. Nam imperdiet ipsum sed placerat pellentesque. Nulla elit
          libero, pharetra non cursus vitae, dictum ut metus. Donec tempor turpis id magna gravida viverra. Aenean sem
          dui, rhoncus non lacus congue, pellentesque lobortis sem. Nullam urna mi, hendrerit a arcu eget, efficitur
          elementum lacus.
          Pellentesque et facilisis est. Vivamus faucibus sem ante, eget feugiat lacus tempor eu. Sed pellentesque nisl
          et lorem sodales egestas. Donec placerat dapibus convallis. Praesent varius mi eget ante tincidunt, quis
          malesuada dui interdum. Nulla pretium mi ut tristique iaculis. Integer nulla felis, tincidunt non arcu vitae,
          gravida varius magna. Nunc ultricies dignissim egestas. Vestibulum tristique, elit in porttitor maximus, risus
          eros ultricies est, non hendrerit quam ante sit amet lorem.</p>

        <p>  Vestibulum quis pulvinar ante. Duis auctor metus nunc, eget vestibulum augue semper a. Etiam semper et magna
          nec tempus. Nam dignissim velit non tempor imperdiet. Sed luctus non urna quis elementum. Sed vitae leo
          tristique, dapibus lectus lacinia, fringilla velit. Class aptent taciti sociosqu ad litora torquent per
          conubia nostra, per inceptos himenaeos. Proin quis velit eget ex mattis faucibus non id elit. Pellentesque
          efficitur ipsum sit amet imperdiet pulvinar. Morbi ac hendrerit massa, ac aliquam lectus. Sed vel lacus nec
          eros malesuada mattis.</p>
      </>
  );
};

export default AboutInfo;