import axios from 'axios';

const axiosOrders = axios.create({
  baseURL: 'https://burgerjs7.firebaseio.com/',
});

export default axiosOrders;