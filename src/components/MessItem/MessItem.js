import React from 'react';
import './MessItem.css';

const MessItem = props => {
  return (
      <div className="mess-item">
        <p>{props.message}</p>
        <button>Read More</button>
      </div>
  );
};

export default MessItem;