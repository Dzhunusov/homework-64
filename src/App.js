import React from 'react';
import {Route, Switch, BrowserRouter, NavLink} from "react-router-dom";
import './App.css';
import Home from "./containers/Home/Home";
import AddMess from "./containers/AddMess/AddMess";
import ContactList from "./containers/ContactList/ContactList";
import AboutInfo from "./containers/AboutInfo/AboutInfo";

const App = () => (
  <BrowserRouter>
    <div className="header">
      <h1>My Blog</h1>
      <div className="nav-block">
        <NavLink to="/" activeClassName="selected" className="nav">Home</NavLink>
        <NavLink to="/add" activeClassName="selected" className="nav">Add</NavLink>
        <NavLink to="/about" activeClassName="selected" className="nav">About</NavLink>
        <NavLink to="/contact" activeClassName="selected" className="nav">Contact</NavLink>
      </div>
    </div>
    <Switch>
      <Route path="/" exact component={Home}/>
      <Route path="/add" component={AddMess}/>
      <Route path="/contact" component={ContactList}/>
      <Route path="/about" component={AboutInfo}/>
      <Route render={() => <h1>404 Not Found</h1>}/>
    </Switch>
  </BrowserRouter>
);

export default App;
